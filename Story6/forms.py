from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    status = forms.CharField(label='Status:', required = True, max_length=300, widget=forms.TextInput(attrs= {'class' : 'form-control'}))
