from django.urls import path
from Story6 import views

urlpatterns = [
    path('', views.index, name = "index"),
    path('add_status/', views.add_status, name = "add_status"),
    path('deleteAll/', views.deleteAll, name= 'deleteAll'),
    path('profile/', views.showProf, name = 'profile'),
]
