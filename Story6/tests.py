from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, showProf
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class story6Test(TestCase):

    def test_myTDDapp_url_is_exsist(self):
            response = Client().get('/Story6/')
            self.assertEqual(response.status_code,200)

    def test_myTDDapp_using_home_template(self):
        response = Client().get('/Story6/')
        self.assertTemplateUsed(response, 'Story6/stats.html')

    def test_myTDDapp_using_index_func(self):
        found = resolve('/Story6/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_activity(self):
        new_activity = Status.objects.create(status = 'Im hungry')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status,1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."],
        )

    def test_story6_post_success_and_render_the_result(self):
        response_post = Client().post('/add_status/', {'status': "hungry"})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/Story6/')
        html_response = response.content.decode('utf8')
        self.assertIn("hungry", html_response)



    def test_profile_url_is_exsist(self):
            response = Client().get('/profile/')
            self.assertEqual(response.status_code,200)

    def test_profile_using_home_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'Story6/profile.html')

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, showProf)

class Lab7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_find_title_element_html(self):
        selenium = self.selenium
        selenium.get('https://afrahsstatuses.herokuapp.com/')
        self.assertIn('Story6', selenium.title)

    def test_find_header_title_html(self):
        selenium = self.selenium
        selenium.get('https://afrahsstatuses.herokuapp.com/')
        header_text = selenium.find_element_by_tag_name('h3').text
        self.assertIn('Halo, Apa Kabar', header_text)

    def test_header_container_using_the_right_margin(self):
        selenium = self.selenium
        selenium.get('https://afrahsstatuses.herokuapp.com/')
        header_css = selenium.find_element_by_class_name('isi').value_of_css_property('margin')
        self.assertIn('10px',header_css)

    def test_header_container_using_the_right_borderradius(self):
        selenium = self.selenium
        selenium.get('https://afrahsstatuses.herokuapp.com/')
        header_css = selenium.find_element_by_class_name('isi').value_of_css_property('border-radius')
        self.assertIn('10px',header_css)

    def test_input_status(self):
        selenium = self.selenium
        selenium.get('https://afrahsstatuses.herokuapp.com/')
        time.sleep(5)

        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        status.send_keys('Coba Coba')
        time.sleep(5)

        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba',self.selenium.page_source)
        time.sleep(5)
