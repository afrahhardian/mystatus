from django.shortcuts import render
from .models import Status
from .forms import Status_Form
from django.http import HttpResponseRedirect

# Create your views here.

response = {}
status_dict = {}
def index(request):
    form = Status_Form()
    statuss = Status.objects.all().values()
    # status_dict = {'statuss': statuss }
    return render(request, 'Story6/stats.html', {"form" : form, "statuss" : statuss})


def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        stat = Status(status=response['status'])
        stat.save()
        return HttpResponseRedirect('/Story6/')
    else:
        return HttpResponseRedirect('/Story6/')

def deleteAll(request):
    status = Status.objects.all()
    status.delete()
    return HttpResponseRedirect('/Story6/')

def showProf(request):
    return render(request, 'Story6/profile.html')
